/***************************************************************************//**
 *   @file   ad77681.h
 *   @brief  Header file of the AD7768-1 Driver.
 *   @author SPopa (stefan.popa@analog.com)
********************************************************************************
 * Copyright 2017(c) Analog Devices, Inc.
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *  - Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *  - Neither the name of Analog Devices, Inc. nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *  - The use of this software may or may not infringe the patent rights
 *    of one or more patent holders.  This license does not release you
 *    from the requirement that you obtain separate licenses from these
 *    patent holders to use this software.
 *  - Use of the software either in source or binary form, must be run
 *    on or directly connected to an Analog Devices Inc. component.
 *
 * THIS SOFTWARE IS PROVIDED BY ANALOG DEVICES "AS IS" AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, NON-INFRINGEMENT,
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL ANALOG DEVICES BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, INTELLECTUAL PROPERTY RIGHTS, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*******************************************************************************/

#ifndef SRC_AD77681_H_
#define SRC_AD77681_H_

#include "main.h"
#pragma anon_unions
/******************************************************************************/
/********************** Macros and Constants Definitions **********************/
/******************************************************************************/
#define	AD77681_REG_CHANNEL_STNDBY					0x00
#define	AD77681_REG_CHANNEL_MODE_A					0x01
#define	AD77681_REG_CHANNEL_MODE_B					0x02
#define	AD77681_REG_CHANNEL_MODE_SELECT			0x03
#define	AD77681_REG_POWER_MODE							0x04
#define	AD77681_REG_GENERAL_CONFIGURATION		0x05
#define	AD77681_REG_DATA_CONTROL						0x06
#define	AD77681_REG_INTERFACE_CONFIGURATION	0x07
#define AD77681_REG_DIST_CONTROLL						0x08
#define AD77681_REG_DEVICE_STATUS						0x09
#define AD77681_REG_REVISION_ID							0x0A

#define AD77681_REG_GPIO_CONTROL						0x0E
#define AD77681_REG_GPIO_WRITE_DATA					0x0F
#define AD77681_REG_GPIO_READ_DATA					0x10
#define AD77681_REG_PRECHARGE_BUFFER_1			0x11
#define AD77681_REG_PRECHARGE_BUFFER_2			0x12
#define AD77681_REG_POSITIVE_PRECHARGE			0x13
#define AD77681_REG_NEGATIEVE_PRECHARGE			0x14

#define AD77681_REG_CH_0_MSB								0x1E
#define AD77681_REG_CH_0_MID								0x1F
#define AD77681_REG_CH_0_LSB								0x20

#define AD77681_REG_CH_1_MSB								0x21
#define AD77681_REG_CH_1_MID								0x22
#define AD77681_REG_CH_1_LSB								0x23

#define AD77681_REG_CH_2_MSB								0x2A
#define AD77681_REG_CH_2_MID								0x2B
#define	AD77681_REG_CH_2_LSB								0x2C

#define	AD77681_REG_CH_3_MSB								0x2D
#define	AD77681_REG_CH_3_MID								0x2E
#define	AD77681_REG_CH_3_LSB								0x2F

#define	AD77681_REG_CH_0_GAIN_MSB						0x36
#define	AD77681_REG_CH_0_GAIN_MID						0x37
#define AD77681_REG_CH_0_GAIN_LSB						0x38
																						
#define	AD77681_REG_CH_1_GAIN_MSB						0x39
#define	AD77681_REG_CH_1_GAIN_MID						0x3A
#define AD77681_REG_CH_1_GAIN_LSB						0x3B
																						
#define	AD77681_REG_CH_2_GAIN_MSB						0x42
#define	AD77681_REG_CH_2_GAIN_MID						0x43
#define	AD77681_REG_CH_2_GAIN_LSB						0x44
																						
#define	AD77681_REG_CH_3_GAIN_MSB						0x45
#define	AD77681_REG_CH_3_GAIN_MID						0x46
#define	AD77681_REG_CH_3_GAIN_LSB						0x47
																						
#define	AD77681_REG_CH_0_SYNC_OFFSET				0x4E
#define	AD77681_REG_CH_1_SYNC_OFFSET				0x4F
#define	AD77681_REG_CH_2_SYNC_OFFSET				0x52
#define	AD77681_REG_CH_3_SYNC_OFFSET				0x53
#define	AD77681_REG_DIAGNOSTIC_RX						0x56
#define	AD77681_REG_DIAGNOSTIC_MUX_CTRL			0x57
#define	AD77681_REG_MODULATOR_DELAY					0x58
#define	AD77681_REG_CHOP_CTRL								0x59

/* AD77681-4 Channel Standby Register 0x00*/
union channel_standby
{
	uint8_t state;
	struct{
		unsigned ch_0:1;
		unsigned ch_1:1;
		unsigned ch_2:1;
		unsigned ch_3:1;
		unsigned zero:4;
	};
};

#define PARAM_CHANNEL_STANDBY_ENABLE 					0
#define PARAM_CHANNEL_STANDBY_STANDBY 				1


/*AD77681-4 Channel Mode A/B 0x01/0x02 */
union channel_mode
{
	uint8_t state;
	struct{
		unsigned dec_rate_a:3;
		unsigned filter_type_a:1;
		unsigned zero:4;
	};
};

#define PARAM_CHANNEL_MODE_DEC_RATE_32 					0
#define PARAM_CHANNEL_MODE_DEC_RATE_64 					1
#define PARAM_CHANNEL_MODE_DEC_RATE_128 				2
#define PARAM_CHANNEL_MODE_DEC_RATE_256 				3
#define PARAM_CHANNEL_MODE_DEC_RATE_512 				4
#define PARAM_CHANNEL_MODE_DEC_RATE_1024 				5

#define PARAM_CHANNEL_MODE_FILTER_SINC5					1
#define PARAM_CHANNEL_MODE_FILTER_WIDEBAND			0

/*AD77681-4 Channel Mode select 0x03*/

union channel_mode_select
{
	uint8_t state;
	struct{
		unsigned ch_0:1;
		unsigned ch_1:1;
		unsigned reserv:2;
		unsigned ch_2:1;
		unsigned ch_3:1;
		unsigned reserved:2;
	};
};

#define PARAM_CHANNEL_MODE_SELECT_A 			0
#define PARAM_CHANNEL_MODE_SELECT_B 			1


/*AD77681-4 POWER MODE 0x04*/

union power_mode
{
	uint8_t state;
	struct{
		unsigned mclk_div:2;
		unsigned unused:1;
		unsigned lvds_en:1;
		unsigned power_mode:2;
		unsigned zero:1;
		unsigned sleep_mode:1;
	};
};

#define PARAM_POWER_MODE_SLEEP_ON 						1
#define PARAM_POWER_MODE_SLEEP_OFF 						0

#define PARAM_POWER_MODE_LOW 									0
#define PARAM_POWER_MODE_MID 									2
#define PARAM_POWER_MODE_FAST 								3

#define PARAM_POWER_MODE_LVDS_ON 							1
#define PARAM_POWER_MODE_LVDS_OFF 						0

#define PARAM_POWER_MODE_MCLK32 							0
#define PARAM_POWER_MODE_MCLK8 								2
#define PARAM_POWER_MODE_MCLK4 								3


/*AD77681-4 GENERAL CONFIGURATION 0x05*/

union general_conf
{
	uint8_t state;
	struct{
		unsigned vcm_vsel:2;
		unsigned one:1;
		unsigned unused:1;
		unsigned vcm_pd:1;
		unsigned retime_en:1;
		unsigned zero:2;
	};
};

#define PARAM_GEN_CONF_VCM_VDD2			0
#define PARAM_GEN_CONF_VCM_165 			1
#define PARAM_GEN_CONF_VCM_25 			2
#define PARAM_GEN_CONF_VCM_214 			3

#define PARAM_GEN_CONF_VCM_PD_OFF 			0
#define PARAM_GEN_CONF_VCM_PD_ON 				1

#define PARAM_GEN_CONF_RETIME_OF 			0
#define PARAM_GEN_CONF_RETIME_ON 			1

/*AD77681-4 DATA CONTROL 0x06*/

union data_control
{
	uint8_t state;
	struct{
		unsigned spi_reset:2;
		unsigned unused:2;
		unsigned single_shot:1;
		unsigned zero:2;
		unsigned spi_sync:1;
	};
};

#define PARAM_DATA_CTRL_SPI_RST_FIRST		3		
#define PARAM_DATA_CTRL_SPI_RST_SECOND	2
#define PARAM_DATA_CTRL_SINGLE_SHOT_ON	1
#define PARAM_DATA_CTRL_SINGLE_SHOT_OFF 0
#define PARAM_DATA_CTRL_SPI_SYNC_LOW		0		
#define PARAM_DATA_CTRL_SPI_SYNC_HIGH		1

/*AD77681-4 INTERFACE CONFIGURATION 0x07*/

union interface_conf
{
	uint8_t state;
	struct{
		unsigned dclk_div:2;
		unsigned crc_sel:2;
		unsigned zero:4;

	};
};

#define PARAM_INTERFACE_CONF_DCLK_DIV1 			3
#define PARAM_INTERFACE_CONF_DCLK_DIV2 			2
#define PARAM_INTERFACE_CONF_DCLK_DIV4 			1
#define PARAM_INTERFACE_CONF_DCLK_DIV8 			0

#define PARAM_INTERFACE_CONF_CRC_OFF 				0
#define PARAM_INTERFACE_CONF_CRC_4SMPL 			1
#define PARAM_INTERFACE_CONF_CRC_16SMPL 		2
#define PARAM_INTERFACE_CONF_CRC__16SMPL	 	3

/*AD77681-4 BIST 0x08*/

union bist_ctrl
{
	uint8_t state;
	struct{
		unsigned start_test:1;
		unsigned zero:7;

	};
};

#define PARAM_BIST_ON 			1
#define PARAM_BIST_OFF 			0

/*AD77681-4 DEVICE STATUS 0x09 read only*/

union dev_state
{
	uint8_t state;
	struct{
		unsigned bist_running:1;
		unsigned bist_pass:1;
		unsigned no_clk:1;
		unsigned chip_error:1;
		unsigned zero:4;

	};
};

#define PARAM_DEVICE_STATUS_BIST_RUNNING			1
#define PARAM_DEVICE_STATUS_BIST_STOPED 			0

#define PARAM_DEVICE_STATUS_BIST_PASS					1
#define PARAM_DEVICE_STATUS_BIST_FAIL 				0

#define PARAM_DEVICE_STATUS_CLK_ERR						1
#define PARAM_DEVICE_STATUS_CLK_OK 						0

#define PARAM_DEVICE_STATUS_CHIP_ERR					1
#define PARAM_DEVICE_STATUS_CHIP_OK 					0


/*AD77681-4 REVISION_ID 0x0A read only*/

union rev_id
{
	uint8_t state;
	struct{
		unsigned rev_id:8;
	};
};

#define PARAM_REV_ID			0x06


/*AD77681-4 GPIO_CONTROL 0x0E */

union gpio_ctrl
{
	uint8_t state;
	struct{
		unsigned gpioe_mod_0:1;
		unsigned gpioe_mod_1:1;
		unsigned gpioe_mod_2:1;
		unsigned gpioe_mod_3:1;
		unsigned gpioe_filter:1;
		unsigned zero:2;
		unsigned gpio_enable:1;

	};
};

#define PARAM_GPIO_ENABLE					1
#define PARAM_GPIO_DISABLE 				0

#define PARAM_GPIO_OUTPUT					1
#define PARAM_GPIO_INPUT 					0

/*AD77681-4 GPIO_WRITE 0x0F */

union gpio_write
{
	uint8_t state;
	struct{
		unsigned gpioe_mod_0:1;
		unsigned gpioe_mod_1:1;
		unsigned gpioe_mod_2:1;
		unsigned gpioe_mod_3:1;
		unsigned gpioe_filter:1;
		unsigned zero:3;
		

	};
};


/*AD77681-4 GPIO_WRITE 0x10 read only*/

union gpio_read
{
	uint8_t state;
	struct{
		unsigned gpioe_mod_0:1;
		unsigned gpioe_mod_1:1;
		unsigned gpioe_mod_2:1;
		unsigned gpioe_mod_3:1;
		unsigned gpioe_filter:1;
		unsigned zero:3;
		

	};
};

/*AD77681-4 PRECHARHE BUFFER 1/2 0x11/0x12  */

union precharge_buf_1
{
	uint8_t state;
	struct{
		unsigned ch_0_precharge_pos:1;
		unsigned ch_0_precharge_neg:1;
		unsigned ch_1_precharge_pos:1;
		unsigned ch_1_precharge_neg:1;
		unsigned resrv:4;
		

	};
};


union precharge_buf_2
{
	uint8_t state;
	struct{
		unsigned ch_2_precharge_pos:1;
		unsigned ch_2_precharge_neg:1;
		unsigned ch_3_precharge_pos:1;
		unsigned ch_3_precharge_neg:1;
		unsigned resrv:4;
		

	};
};
#define PARAM_PRECHARGE_BUF_ON					1
#define PARAM_PRECHARGE_BUF_OFF 				0


/*AD77681-4 POSITIV/NEGATIV REFERENCE BUFFER 1/2 0x13/0x14  */

union positiv_ref_buf
{
	uint8_t state;
	struct{
		unsigned ch_0_positiv_ref:1;
		unsigned ch_1_positiv_ref:1;
		unsigned unused:2;
		unsigned ch_2_positiv_ref:1;
		unsigned ch_3_positiv_ref:1;
		unsigned resrv:2;
		

	};
};


union negativ_ref_buf
{
	uint8_t state;
	struct{
		unsigned ch_0_negativ_ref:1;
		unsigned ch_1_negativ_ref:1;
		unsigned unused:2;
		unsigned ch_2_negativ_ref:1;
		unsigned ch_3_negativ_ref:1;
		unsigned resrv:2;
		

	};
};
#define PARAM_POSITIV_NEGATIV_BUF_ON					1
#define PARAM_POSITIV_NEGATIV_BUF_OFF 				0


/*AD77681-4 DIAGNOSTIC RX 0x56 */

union diag_rx
{
	uint8_t state;
	struct{
		unsigned ch_0_rx:1;
		unsigned ch_1_rx:1;
		unsigned reserv:2;
		unsigned ch_2_rx:1;
		unsigned ch_3_rx:1;
		unsigned zero:2;


	};
};

#define PARAM_DIAG_RX_RECEIEVED				1
#define PARAM_DIAG_RX_DISABLED 				0

/*AD77681-4 DIAGNOSTIC MUX 0x56 */

union diag_mux
{
	uint8_t state;
	struct{
		unsigned grpa_sel:3;
		unsigned reserv:1;
		unsigned grpb_sel:3;
		unsigned zero:1;


	};
};

#define PARAM_DIAG_MUX_ZERO						5
#define PARAM_DIAG_MUX_NEG_FULL				4
#define PARAM_DIAG_MUX_POS_FULL				3
#define PARAM_DIAG_MUX_OFF						0


/*AD77681-4 Modulator delay 0x58 */

union mod_delay
{
	uint8_t state;
	struct{

		unsigned only_2:2;
		unsigned clk_mod_delay:2;
		unsigned zero:4;


	};
};

#define PARAM_MOD_DELAY_OFF						0
#define PARAM_MOD_DELAY_CH_1_2				1
#define PARAM_MOD_DELAY_CH_3_4				2
#define PARAM_MOD_DELAY_ALL_CH0				3

/*AD77681-4 CHOP CONTROL 0x58 */

union chop_ctrl
{
	uint8_t state;
	struct{

		unsigned grpb_chop:2;
		unsigned grpa_chop:2;
		unsigned zero:4;


	};
};

#define PARAM_CHOP_DIV8					1
#define PARAM_CHOP_DIV32				2


union long_param
{
	uint32_t param;
	struct{
		unsigned lsb:8;
		unsigned mid:8;
		unsigned msb:8;
		unsigned zero:8;
		

	};
};

/*****************************************************************************/
/*************************** Types Declarations *******************************/
/******************************************************************************/
#define DEFAULT_CH_STANDBY  										0x00
#define DEFAULT_CH_MODE_A												0x0D
#define DEFAULT_CH_MODE_B												0x0D
#define DEFAULT_CH_MODE_SEL											0x00
#define DEFAULT_POWER_MODE											0x00
#define DEFAULT_GENERAL_CONF										0x08
#define DEFAULT_DATA_CTRL												0x80
#define DEFAULT_INTERFACE_CONF									0x00
#define DEFAULT_BIST_CONTROL										0x00
#define DEFAULT_DEVICE_STAT											0x00
#define DEFAULT_REVISION_ID											0x06
#define DEFAULT_GPIO_CTRL												0x00
#define DEFAULT_GPIO_WRITE											0x00
#define DEFAULT_GPIO_READ												0x00
#define DEFAULT_PRECHARDGE_BUF_1								0xFF
#define DEFAULT_PRECHARDGE_BUF_2								0xFF
#define DEFAULT_POSITIF_REF_PRECHARDGE					0x00
#define DEFAULT_NEG_REF_PRECHARDGE							0x00
#define DEFAULT_DIAG_RX													0x00
#define DEFAULT_DIAG_MUX												0x00
#define DEFAULT_DELAY														0x02
#define DEFAULT_CHOP_CTRL												0x0A


/******************************************************************************/
/************************ Functions Declarations ******************************/
/******************************************************************************/

uint8_t AD77681_Write_Register (uint8_t Register_Address, uint8_t Data);
uint8_t AD77681_Read_Register (uint8_t Register_Address);
uint8_t AD77681_Soft_Reset (void);
uint8_t AD77681_Init (void);
uint8_t AD77681_CONFIG_Interface (union interface_conf conf);
uint8_t AD77681_CONFIG_POWER_MODE (union power_mode conf);
uint8_t AD77681_CONFIG_DIAGNOSTIC (union diag_mux conf);
uint8_t AD77681_CONFIG_DIAGNOSTIC_RX (union diag_rx conf);
uint8_t AD77681_CONFIG_CHANNEL_MODE_A (union channel_mode conf);
uint8_t AD77681_Channel_Offset (uint8_t nmbr_channel, uint32_t offset);
uint8_t AD77681_Channel_Gain (uint8_t nmbr_channel, uint32_t gain);
uint8_t AD77681_CONFIG_PRECHARDGE_BUFF (union precharge_buf_1 conf1, union precharge_buf_2 conf2);
#endif /* SRC_AD77681_H_ */
