/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "dma.h"
#include "sai.h"
#include "spi.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "ad77681.h"
#include "math.h"
#pragma import(__use_no_semihosting_swi)
#include <stdio.h>
extern UART_HandleTypeDef huart3;
#define ITM_Port8(n)    (*((volatile unsigned char *)(0xE0000000+4*n)))
#define ITM_Port16(n)   (*((volatile unsigned short*)(0xE0000000+4*n)))
#define ITM_Port32(n)   (*((volatile unsigned long *)(0xE0000000+4*n)))

#define DEMCR           (*((volatile unsigned long *)(0xE000EDFC)))
#define TRCENA          0x01000000

struct __FILE { int handle; /* Add whatever you need here */ };
FILE __stdout;
FILE __stdin;

int fputc(int ch, FILE *f) {
  HAL_UART_Transmit(&huart3, (uint8_t *)&ch, 1, 0xFFFF);


  return(ch);
}
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */

union adc_data_u{
uint8_t adc_data[16];
uint32_t adc_sample[4];
	struct{
		unsigned data_0:24;
		unsigned channel_id_0:3;
		unsigned filter_saturated_0:1;
		unsigned filter_type_0:1;
		unsigned repited_data_0:1;
		unsigned filter_not_setted_0:1;
		unsigned error_flag_0:1;
		
		
		unsigned data_1:24;
		unsigned channel_id_1:3;
		unsigned filter_saturated_1:1;
		unsigned filter_type_1:1;
		unsigned repited_data_1:1;
		unsigned filter_not_setted_1:1;
		unsigned error_flag_1:1;
		
		
		unsigned data_2:24;
		unsigned channel_id_2:3;
		unsigned filter_saturated_2:1;
		unsigned filter_type_2:1;
		unsigned repited_data_2:1;
		unsigned filter_not_setted_2:1;
		unsigned error_flag_2:1;
		
		
		unsigned data_3:24;
		unsigned channel_id_3:3;
		unsigned filter_saturated_3:1;
		unsigned filter_type_3:1;
		unsigned repited_data_3:1;
		unsigned filter_not_setted_3:1;
		unsigned error_flag_3:1;
	};
};
uint8_t			adc_data[5];
uint8_t res;
union adc_data_u adc;
union diag_mux diag_mux;
union diag_rx diag_rx;
union precharge_buf_1 pre_buf_1;
union precharge_buf_2 pre_buf_2;
int test_point;
float voltage;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */
  

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_SPI4_Init();
  MX_USART3_UART_Init();
  MX_SAI1_Init();
  /* USER CODE BEGIN 2 */
	if(AD77681_Init() == 0)
	{
		printf("%s","ADC_Init_success");
	}
	HAL_Delay(150);
	diag_mux.grpa_sel = PARAM_DIAG_MUX_ZERO;
	AD77681_CONFIG_DIAGNOSTIC(diag_mux);
	HAL_Delay(100);
	diag_rx.ch_0_rx = PARAM_DIAG_RX_DISABLED;
	diag_rx.ch_1_rx = PARAM_DIAG_RX_DISABLED;
	diag_rx.ch_2_rx = PARAM_DIAG_RX_DISABLED;
	diag_rx.ch_3_rx = PARAM_DIAG_RX_DISABLED;
	AD77681_CONFIG_DIAGNOSTIC_RX(diag_rx);
	HAL_Delay(100);
	
//	AD77681_Channel_Offset(0,0x0037FFFF);
//	AD77681_Channel_Gain(0,0x007FFFFF);
//	
//	AD77681_Channel_Offset(1,0x00000000);
//	AD77681_Channel_Gain(1,0x006FFFFF);
//	
//	AD77681_Channel_Offset(2,0x00000000);
		AD77681_Channel_Gain(2,0x007FFFFF);
	
//	AD77681_Channel_Offset(3,0x00000000);
//	AD77681_Channel_Gain(3,0x00FFFFF);
	
//	pre_buf_1.state = 0xFF;
//	pre_buf_2.state = 0xFF;
//	AD77681_CONFIG_PRECHARDGE_BUFF(pre_buf_1, pre_buf_2);
	HAL_SAI_Receive_DMA(&hsai_BlockA1,adc.adc_data,4);//Прием данных
		


  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInitStruct = {0};

  /** Configure the main internal regulator output voltage 
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_BYPASS;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 4;
  RCC_OscInitStruct.PLL.PLLN = 180;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 7;
  RCC_OscInitStruct.PLL.PLLR = 2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Activate the Over-Drive mode 
  */
  if (HAL_PWREx_EnableOverDrive() != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_5) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInitStruct.PeriphClockSelection = RCC_PERIPHCLK_SAI1;
  PeriphClkInitStruct.PLLSAI.PLLSAIM = 4;
  PeriphClkInitStruct.PLLSAI.PLLSAIN = 200;
  PeriphClkInitStruct.PLLSAI.PLLSAIQ = 2;
  PeriphClkInitStruct.PLLSAI.PLLSAIP = RCC_PLLSAIP_DIV2;
  PeriphClkInitStruct.PLLSAIDivQ = 1;
  PeriphClkInitStruct.Sai1ClockSelection = RCC_SAI1CLKSOURCE_PLLSAI;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */
void HAL_SAI_RxCpltCallback(SAI_HandleTypeDef *hsai)
{

		//printf("%x",adc.adc_sample[i]);
	
	
		printf("%d:",(adc.adc_sample[0]&0x00800000) ? (adc.adc_sample[0]|0xFF000000) : (adc.adc_sample[0]&0x00FFFFFF));
		
		printf("%d:",(adc.adc_sample[1]&0x00800000) ? (adc.adc_sample[1]|0xFF000000) : (adc.adc_sample[1]&0x00FFFFFF));
		
		printf("%d:",(adc.adc_sample[2]&0x00800000) ? (adc.adc_sample[2]|0xFF000000) : (adc.adc_sample[2]&0x00FFFFFF));
		
		printf("%d\r\n",(adc.adc_sample[3]&0x00800000) ? (adc.adc_sample[3]|0xFF000000) : (adc.adc_sample[3]&0x00FFFFFF));
		
		test_point = (adc.adc_sample[2]&0x00800000) ? (adc.adc_sample[2]|0xFF000000) : (adc.adc_sample[2]&0x00FFFFFF);
		voltage = (test_point/(pow(2,23)))*5; //либо 4.095  Vref
}




/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
