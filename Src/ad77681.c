/***************************************************************************//**
 *   @file   ad77681.c
 *   @brief  Implementation of AD7768-1 Driver.
 *   @author SPopa (stefan.popa@analog.com)
********************************************************************************
 * Copyright 2017(c) Analog Devices, Inc.
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *  - Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *  - Neither the name of Analog Devices, Inc. nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *  - The use of this software may or may not infringe the patent rights
 *    of one or more patent holders.  This license does not release you
 *    from the requirement that you obtain separate licenses from these
 *    patent holders to use this software.
 *  - Use of the software either in source or binary form, must be run
 *    on or directly connected to an Analog Devices Inc. component.
 *
 * THIS SOFTWARE IS PROVIDED BY ANALOG DEVICES "AS IS" AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, NON-INFRINGEMENT,
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL ANALOG DEVICES BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, INTELLECTUAL PROPERTY RIGHTS, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*******************************************************************************/

/******************************************************************************/
/***************************** Include Files **********************************/
/******************************************************************************/
#include "stdio.h"
#include "stdlib.h"
#include "stdbool.h"
#include <string.h>
#include "ad77681.h"

/******************************************************************************/
/************************** Functions Implementation **************************/

 extern SPI_HandleTypeDef hspi4;

uint8_t AD77681_Write_Register (uint8_t Register_Address, uint8_t Data)
{
	uint8_t result = 0;
	uint8_t Byte[2];
	Byte[0] = Register_Address;
	Byte[1] = Data;
	cs_low;
	if(HAL_SPI_TransmitReceive(&hspi4, (uint8_t*) Byte, (uint8_t*) &result, 2, 0x1000) != HAL_OK)
  {
    Error_Handler();
  }
	cs_high;
	return result;
}

uint8_t AD77681_Read_Register (uint8_t Register_Address)
{
	uint8_t result[2];
	uint8_t trash;
	uint8_t Byte[2];
	Byte[0] = (Register_Address|0x80);
	Byte[1] = 0x00;
	cs_low;
	if(HAL_SPI_TransmitReceive(&hspi4,  Byte, &trash, 2, 0x1000) != HAL_OK)
  {
    Error_Handler();
  }
	cs_high;

	cs_low;
	if(HAL_SPI_TransmitReceive(&hspi4,  Byte, result, 2, 0x1000) != HAL_OK)
  {
    Error_Handler();
  }
	cs_high;
	return result[1];
}

uint8_t AD77681_Soft_Reset (void)
{
	uint8_t result =0;
	union data_control data_ctrl;
	data_ctrl.spi_reset = 0;
	AD77681_Write_Register(AD77681_REG_DATA_CONTROL,data_ctrl.state); //после аппартного резета пустая команда чтобы схавать E0;
	
	data_ctrl.spi_reset = PARAM_DATA_CTRL_SPI_RST_FIRST;
	result |= AD77681_Write_Register(AD77681_REG_DATA_CONTROL,data_ctrl.state);
	
	data_ctrl.spi_reset = PARAM_DATA_CTRL_SPI_RST_SECOND;
	result |= AD77681_Write_Register(AD77681_REG_DATA_CONTROL,data_ctrl.state);
	
	data_ctrl.spi_reset = 0;
	AD77681_Write_Register(AD77681_REG_DATA_CONTROL,data_ctrl.state); //после софтверного резета пустая команда чтобы схавать E0;
	return result;
}

uint8_t AD77681_CONFIG_Interface (union interface_conf conf)
{
	uint8_t result =0;
	
	result |= AD77681_Write_Register(AD77681_REG_INTERFACE_CONFIGURATION,conf.state);
	
	return result;
}

uint8_t AD77681_CONFIG_POWER_MODE (union power_mode conf)
{
	uint8_t result =0;
	
	result |= AD77681_Write_Register(AD77681_REG_POWER_MODE,conf.state);
	
	return result;
}

uint8_t AD77681_CONFIG_DIAGNOSTIC (union diag_mux conf)
{
	uint8_t result =0;
	
	result |= AD77681_Write_Register(AD77681_REG_DIAGNOSTIC_MUX_CTRL,conf.state);
	
	return result;
}


uint8_t AD77681_CONFIG_DIAGNOSTIC_RX (union diag_rx conf)
{
	uint8_t result =0;
	
	result |= AD77681_Write_Register(AD77681_REG_DIAGNOSTIC_RX,conf.state);
	
	return result;
}

uint8_t AD77681_CONFIG_CHANNEL_MODE_A (union channel_mode conf)
{
	uint8_t result =0;
	
	result |= AD77681_Write_Register(AD77681_REG_CHANNEL_MODE_A,conf.state);
	
	return result;
}

uint8_t AD77681_CONFIG_PRECHARDGE_BUFF (union precharge_buf_1 conf1, union precharge_buf_2 conf2)
{
	uint8_t result =0;
	
	result |= AD77681_Write_Register(AD77681_REG_PRECHARGE_BUFFER_1,conf1.state);
	HAL_Delay(100);
	result |= AD77681_Write_Register(AD77681_REG_PRECHARGE_BUFFER_2,conf2.state);
	return result;
}
uint8_t AD77681_Channel_Gain (uint8_t nmbr_channel, uint32_t gain)
{
	union long_param param;
	uint8_t result =0;
	param.param = gain;
	switch (nmbr_channel)
	{
		case 0:
			result |= AD77681_Write_Register(AD77681_REG_CH_0_GAIN_LSB,param.lsb);
			result |= AD77681_Write_Register(AD77681_REG_CH_0_GAIN_MID,param.mid);
			result |= AD77681_Write_Register(AD77681_REG_CH_0_GAIN_MSB,param.msb);
			break;
		case 1:
			result |= AD77681_Write_Register(AD77681_REG_CH_1_GAIN_LSB,param.lsb);
			result |= AD77681_Write_Register(AD77681_REG_CH_1_GAIN_MID,param.mid);
			result |= AD77681_Write_Register(AD77681_REG_CH_1_GAIN_MSB,param.msb);
			break;
		case 2:
			result |= AD77681_Write_Register(AD77681_REG_CH_2_GAIN_LSB,param.lsb);
			result |= AD77681_Write_Register(AD77681_REG_CH_2_GAIN_MID,param.mid);
			result |= AD77681_Write_Register(AD77681_REG_CH_2_GAIN_MSB,param.msb);
			break;
		case 3:
			result |= AD77681_Write_Register(AD77681_REG_CH_3_GAIN_LSB,param.lsb);
			result |= AD77681_Write_Register(AD77681_REG_CH_3_GAIN_MID,param.mid);
			result |= AD77681_Write_Register(AD77681_REG_CH_3_GAIN_MSB,param.msb);
			break;
		default:
			result = 1;
			break;
	}
	return result;
}


uint8_t AD77681_Channel_Offset (uint8_t nmbr_channel, uint32_t offset)
{
	union long_param param;
	uint8_t result =0;
	param.param = offset;
	switch (nmbr_channel)
	{
		case 0:
			result |= AD77681_Write_Register(AD77681_REG_CH_0_LSB,param.lsb);
			result |= AD77681_Write_Register(AD77681_REG_CH_0_MID,param.mid);
			result |= AD77681_Write_Register(AD77681_REG_CH_0_MSB,param.msb);
			break;
		case 1:
			result |= AD77681_Write_Register(AD77681_REG_CH_1_LSB,param.lsb);
			result |= AD77681_Write_Register(AD77681_REG_CH_1_MID,param.mid);
			result |= AD77681_Write_Register(AD77681_REG_CH_1_MSB,param.msb);
			break;
		case 2:
			result |= AD77681_Write_Register(AD77681_REG_CH_2_LSB,param.lsb);
			result |= AD77681_Write_Register(AD77681_REG_CH_2_MID,param.mid);
			result |= AD77681_Write_Register(AD77681_REG_CH_2_MSB,param.msb);
			break;
		case 3:
			result |= AD77681_Write_Register(AD77681_REG_CH_3_LSB,param.lsb);
			result |= AD77681_Write_Register(AD77681_REG_CH_3_MID,param.mid);
			result |= AD77681_Write_Register(AD77681_REG_CH_3_MSB,param.msb);
			break;
		default:
			result = 1;
			break;
	}
	return result;
}
uint8_t AD77681_Init (void)
{
	uint8_t result=0;
	union interface_conf interface_cfg;
	union power_mode power_cfg;
	/* Дефолтная конфигурация*/
	power_cfg.state = DEFAULT_POWER_MODE;
	power_cfg.mclk_div = PARAM_POWER_MODE_MCLK4;
	power_cfg.power_mode = PARAM_POWER_MODE_FAST;
	
	interface_cfg.state = DEFAULT_INTERFACE_CONF;
	interface_cfg.dclk_div = PARAM_INTERFACE_CONF_DCLK_DIV4;
	
	HAL_GPIO_WritePin(RESET_ADC_GPIO_Port, RESET_ADC_Pin, GPIO_PIN_RESET);
	HAL_Delay(100);
	HAL_GPIO_WritePin(RESET_ADC_GPIO_Port, RESET_ADC_Pin, GPIO_PIN_SET); //HARD_RESET
	HAL_Delay(200);
	

	
	result |= AD77681_Soft_Reset();
	HAL_Delay(100);
	result |=AD77681_CONFIG_POWER_MODE(power_cfg);
	HAL_Delay(100);
	result |=AD77681_CONFIG_Interface(interface_cfg);
	HAL_Delay(100);
	
	return result;
}

